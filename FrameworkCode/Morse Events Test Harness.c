#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Port.h"
#include "termio.h"
#include "EnablePA25_PB23_PD7_PF0.h"

//Include Files I wrote
#include "ShiftRegisterWrite.h"
#include "MorseElementsService.h"

#define clrScrn() printf("\x1b[2J")
#define goHome() printf("\x1b[1,1H")
#define clrLine() printf("\x1b[K")



int main(void)
{
  puts("Start CheckMorseEvents");
  SR_Init();
  while(true)
  {      
  CheckMorseEvents();
  }
  return 0;
}