/****************************************************************************
 Module
   ButtonDebouncer.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 11/01/18       ndc      Implement button Debouncer
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "TemplateFSM.h"
#include "ButtonDebouncer.h"
#include "MorseElementsService.h"
#include "ShiftRegisterWrite.h"
#include "ES_Timers.h"
#include "driverlib/gpio.h"
#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_sysctl.h"
#include <stdint.h>
#include <stdbool.h>
#include "termio.h"
#include "LCD_Write.h"

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static ButtonStates_t CurrentState;
static uint8_t        LastButtonState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t  MyPriority;
static uint8_t  BTTN_MASK = 0x10;
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitButtonDebounceSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitButtonDebounceSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial Debouncing state and sample last button state
  CurrentState = Ready4DB;
  LastButtonState = (HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) & BTTN_MASK);
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  //Set Timers
  ES_Timer_InitTimer(DEBOUNCE_TIMER, 100);
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostButtonDebounceSM

 Parameters
     EF_Event ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostButtonDebounceSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunButtonDebounceFSM

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunButtonDebounceSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState)
  {
    case Debouncing:        // If current state is initial Psedudo State
    {
      puts("Enter Debouncing case\r\n");
      if ((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == DEBOUNCE_TIMER))
      {
        //Update CurrentState to Ready4DB
        CurrentState = Ready4DB;
      }
      //CurrentState = Ready4DB;
    }
    break;

    case Ready4DB:
    {
      puts("Enter Ready4DB case\r\n");
      if (ThisEvent.EventType == ES_BUTTON_UP)
      {
        ES_Timer_InitTimer(DEBOUNCE_TIMER, 100);

        //Update Current State
        CurrentState = Debouncing;
        //Create Event
        ES_Event_t ThisEvent;
        ThisEvent.EventType = ES_DBBUTTON_UP;
      }
      if (ThisEvent.EventType == ES_BUTTON_DOWN)
      {
        ES_Timer_InitTimer(DEBOUNCE_TIMER, 100);
        //Update Current State
        CurrentState = Debouncing;
        //Create Event
        ES_Event_t ThisEvent;
        puts("Detected Button Down\r\n");
        ThisEvent.EventType = ES_DBBUTTON_DOWN;
        PostMorseElementsService(ThisEvent); //Want to reset CurrentState of MorseElementsService to CalWaitForRise
      }
    }
    break;
  }
  // end switch on Current State
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryTemplateSM

 Parameters
     None

 Returns
     TemplateState_t The current state of the Template state machine

 Description
     returns the current state of the Template state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
ButtonStates_t QueryTemplateFSM(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/
/****************************************************************************
 Function
   Check4Button
 Parameters
   None
 Returns
   bool: true if a new event was detected
 Description
   Sample event checker grabbed from the simple lock state machine example
 Notes
   will not compile, sample only
 Author
   J. Edward Carryer, 08/06/13, 13:48
****************************************************************************/
bool Check4Button(void)
{
  uint8_t CurrentButtonState;
  bool    ReturnVal = false;
  //puts("Entered Check4Button");
  CurrentButtonState = (HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) & BTTN_MASK);
  //Check for change
  if (CurrentButtonState != LastButtonState)
  {
    if (CurrentButtonState == 0)  // If Button Depressed
    {
      ES_Event_t ThisEvent;
      ThisEvent.EventType = ES_BUTTON_DOWN;
      //puts("Button Down\n\r");
      PostButtonDebounceSM(ThisEvent);
      ReturnVal = true;
    }
    else   // event detected, so post detected event
    {
      ES_Event_t ThisEvent;
      ThisEvent.EventType = ES_BUTTON_UP;
      PostButtonDebounceSM(ThisEvent);
      ReturnVal = true;
    }
  }
  LastButtonState = CurrentButtonState; // update the state for next time

  return ReturnVal;
}
