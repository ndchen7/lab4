#define TEST

#ifdef TEST /* test Harness for testing this module */

#include "termio.h"
#include "LCD_Write.h"
#include "ShiftRegisterWrite.h"
#include "BITDEFS.H"
#include "ES_Port.h"

int main(void)
{
  //Initialize hardware
  TERMIO_Init();
  LCD_HWInit();

/**********************************************************/
  //Test Harnessfor LCD_Write Test Harness
  puts("\r\n LCD_Write Test Harness\r\n");

  //SR_Write(0x55);
  //LCD_WriteCommand8(0x49);//Should See 1001 on ports 4-7
  //LCD_WriteCommand8(0xAA);//Should See 1010 on ports 4-7
  //LCD_WriteData8(0xAA);
/**********************************************************/
//Test Harness For Initialization

  volatile uint8_t hit_counter = 0;
  while (!kbhit())
  {
    getchar();
    hit_counter++;
    if (hit_counter >= 12)
    {
      puts("Completed Initialization\r\n");
      break;
    }
    LCD_TakeInitStep();
  }

  //Test to write characters

  while (!kbhit())
  {
    LCD_WriteData8((uint8_t)getchar());
    puts("Wrote character");
  }

  //LCD_TakeInitStep();
  printf("\n");
  puts("\r\n DONE\r\n");
  return 0;
}

#endif
