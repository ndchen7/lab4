/****************************************************************************
 Module
   TemplateFSM.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 11/02/18       ndc      Implement Morse Decode Service
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include <string.h>
#include "MorseDecodeService.h"
#include "LCDService.h"

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/
char DecodeMorseString(void);
/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static char MorseString[8];
char        legalChars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890?.,:'-/()\"= !$&+;@_";
char        morseCode[][8] = { ".-", "-...", "-.-.", "-..", ".", "..-.", "--.",
                               "....", "..", ".---", "-.-", ".-..", "--", "-.", "---",
                               ".--.", "--.-", ".-.", "...", "-", "..-", "...-",
                               ".--", "-..-", "-.--", "--..", ".----", "..---",
                               "...--", "....-", ".....", "-....", "--...", "---..",
                               "----.", "-----", "..--..", ".-.-.-", "--..--",
                               "---...", ".----.", "-....-", "-..-.", "-.--.-",
                               "-.--.-", ".-..-.", "-...-", "-.-.--", "...-..-",
                               ".-...", ".-.-.", "-.-.-.", ".--.-.", "..--.-" };

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitMorseDecodeService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitMorseDecodeService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostMorseDecodeService

 Parameters
     EF_Event ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostMorseDecodeService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunMorseDecodeService

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunMorseDecodeService(ES_Event_t ThisEvent)
{
  static uint8_t  index;
  //puts("Entered Run Morse");
  ES_Event_t      ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

//	If ThisEvent is DotDetected Event
  if (ThisEvent.EventType == ES_DOT_DETECTED)
  {
    //puts("Dot Detected");
//		If there is room for another Morse element in the internal representation
    if (strlen(MorseString) < 8)
    {
      index = strlen(MorseString);
      //			Add a Dot to the internal representation
      MorseString[index] = '.';
    }
    else
    {
//			Set ReturnValue to ES_ERROR with param set to indicate this location
      ReturnEvent.EventType = ES_ERROR;
      ReturnEvent.EventParam = strlen(MorseString);
    }
//	End if ThisEvent is DotDetected Event
  }
//	If ThisEvent is DashDetected Event
  if (ThisEvent.EventType == ES_DASH_DETECTED)
  {
    //puts("Dash Detected");
//		If there is room for another Morse element in the internal representation
    if (strlen(MorseString) < 8)
    {
      index = strlen(MorseString);
      //			Add a Dot to the internal representation
      MorseString[index] = '-';
    }
    else
    {
//			Set ReturnValue to ES_ERROR with param set to indicate this location
      ReturnEvent.EventType = ES_ERROR;
      ReturnEvent.EventParam = strlen(MorseString);
    }
  }
//	End if ThisEvent is DashDetected Event

//	If ThisEvent is EOCDetected Event
  if (ThisEvent.EventType == ES_EOC_DETECTED)
  {
    //puts("EOC Detected");
//		call DecodeMorse to try and match current MorseString
    ES_Event_t  ReturnEvent;
    ReturnEvent.EventType = ES_LCD_PUTCHAR;
    char        character = DecodeMorseString();
    printf("%c", character);
    ReturnEvent.EventParam = character;
//		Print to LCD the decoded character
    PostLCDService(ReturnEvent);

//		Clear (empty) the MorseString variable
    for (int i = 0; i < 8; i++)
    {
      MorseString[i] = '\0';
    }
  }
//	End if is EOCDetected Event

//	If ThisEvent is EOWDetected Event
  if (ThisEvent.EventType == ES_EOW_DETECTED)
  {
//		call DecodeMorse to try and match current MorseString
    ES_Event_t ReturnEvent;
    ReturnEvent.EventType = ES_LCD_PUTCHAR;
    //		Print to LCD the decoded character
    char character = DecodeMorseString();
    printf("%c", character);
    ReturnEvent.EventParam = character;
    PostLCDService(ReturnEvent);
    puts("\r\n");
//		Print to the LCD a space
    ReturnEvent.EventParam = ' ';
    PostLCDService(ReturnEvent);
//		Clear (empty) the MorseString variable
    for (int i = 0; i < 8; i++)
    {
      MorseString[i] = '\0';
    }
  }
//	End if ThisEvent is EOWDetected Event

//	If ThisEvent is BadSpace Event  (Added 10/19/15 JEC)
  if (ThisEvent.EventType == ES_BAD_SPACE)
  {
//		Clear (empty) the MorseString variable
    for (int i = 0; i < 8; i++)
    {
      MorseString[i] = '\0';
    }
  }
//	End if ThisEvent is BadSpace Event

//	If ThisEvent is BadPulse Event  (Added 10/19/15 JEC)
  if (ThisEvent.EventType == ES_BAD_PULSE)
  {
//		Clear (empty) the MorseString variable
    for (int i = 0; i < 8; i++)
    {
      MorseString[i] = '\0';
    }
  }
//	End if ThisEvent is BadPulse Event

//	If ThisEvent is DBButtonDown Event  (Added 11/3/11 JEC)
  if (ThisEvent.EventType == ES_DBBUTTON_DOWN)
  {
//		Clear (empty) the MorseString variable
    for (int i = 0; i < 8; i++)
    {
      MorseString[i] = '\0';
    }
  }
//	End if ThisEvent is ButtonDown Event

  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/
char DecodeMorseString(void)
//Takes no parameters, returns either a character or a '~' indicating failure
{
  //return '~', if we didn't find a matching string in MorseCode
  char ReturnValue = '~';
  //For every entry in the array MorseCode
  for (int i = 0; i < strlen(legalChars); i++)
  {
    //If MorseString is the same as current position in MorseCode
    //return contents of current position in LegalChars
    if (strcmp(&morseCode[i][0], MorseString) == 0)
    {
      ReturnValue = legalChars[i];
    }
  }
  return ReturnValue;
}
