/****************************************************************************
 Module
   MorseElementsService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 11/01/18       ndc      Implement MorseElementsService
****************************************************************************/

//Pseudo-code for the Morse Elements module (a service that implements a state machine)
//Data private to the module: MyPriority, CurrentState, TimeOfLastRise, TimeOfLastFall, LengthOfDot, FirstDelta, LastInputState
//----------------------------- Include Files -----------------------------*/
// the common headers for C99 types
#include "ES_Configure.h"
#include "BITDEFS.H"
#include "LCD_Write.h"
#include "driverlib/gpio.h"
#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_sysctl.h"
#include <stdint.h>
#include <stdbool.h>
#include "termio.h"

#include "BITDEFS.H"
#include "ShiftRegisterWrite.h"
#include "LCD_Write.h"
/* include header files for this service
*/
#include "MorseElementsService.h"
#include "MorseDecodeService.h"

/* include header files for hardware access
*/
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"
#include "ES_Events.h"
/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t              MyPriority;
static MorseElementsState_t CurrentState = InitMorseElements;
static MorseElementsState_t NextState;
static uint8_t              LastInputState;
static uint16_t             LengthOfDot, TimeOfLastRise, TimeOfLastFall, FirstDelta;
// add a deferral queue for up to 3 pending deferrals +1 to allow for overhead
static ES_Event_t           DeferralQueue[3 + 1];

#define MASK 0x08
//Prototype function
static void TestCalibration(void);
static void CharacterizeSpace(void);
static void CharacterizePulse(void);

/*------------------------------ Module Code ------------------------------*/
/*****************************************************************************/
//InitializeMorseElements
//Takes a priority number, returns True.

bool InitMorseElementsService(uint8_t Priority)
{
  ES_Event_t ThisEvent;
//Initialize the MyPriority variable with the passed in parameter.
  MyPriority = Priority;
//Initialize the port line to receive Morse code. Done in SR_Write
  //LCD_HWInit();
  //init deferral queue
  ES_InitDeferralQueueWith(DeferralQueue, ARRAY_SIZE(DeferralQueue));
  //Init short Timer for Channel A
  ES_ShortTimerInit(MyPriority, SHORT_TIMER_UNUSED);
//Sample port line and use it to initialize the LastInputState variable
  LastInputState = (HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) & MASK);
//Set CurrentState to be InitMorseElements
  CurrentState = InitMorseElements;
//Set FirstDelta to 0
  FirstDelta = 0;
//Post Event ES_Init to MorseElements queue (this service)
  ThisEvent.EventType = ES_INIT;
  //printf("I Successfully Initialized\n\r");

  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

//End of InitializeMorseElements

/*****************************************************************************/
//CheckMorseEvents
//Takes no parameters, returns True if an event was posted (11/4/11 jec)
//local RetrunVal = False, CurrentInputState

bool CheckMorseEvents(void)
{
  uint16_t  CurrentTime;
  bool      ReturnVal = false;
  uint8_t   CurrentInputState;
//Get the CurrentInputState from the input line
  CurrentInputState = (HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) & MASK);
  //puts("Enter CheckMorseEvents");

//If the state of the Morse input line has changed
  if (CurrentInputState != LastInputState)
  {
    //puts("Current != Last");
    //		If the current state of the input line is high
    if (CurrentInputState == 0)
    {
//			PostEvent ES_FALLING_EDGE with parameter of the Current Time
      CurrentTime = ES_Timer_GetTime();
      ES_Event_t ThisEvent;
      ThisEvent.EventType = ES_FALLING_EDGE;
      ThisEvent.EventParam = CurrentTime;
      PostMorseElementsService(ThisEvent);
      //puts("R");
    }
//		Else (current input state is low)
    else
    {
//			PostEvent ES_RISING_EDGE with parameter of the Current Time
      CurrentTime = ES_Timer_GetTime();
      ES_Event_t ThisEvent;
      ThisEvent.EventType = ES_RISING_EDGE;
      ThisEvent.EventParam = CurrentTime;
      PostMorseElementsService(ThisEvent);
      //puts("F");
    }
//		Endif
//		Set ReturnVal = True
    ReturnVal = true;
//Endif
  }
//Set LastInputState to CurrentInputState
  LastInputState = CurrentInputState;
//Return ReturnVal
  return ReturnVal;
}

//End of CheckMorseEvents

/*****************************************************************************/
bool PostMorseElementsService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/*****************************************************************************/
//RunMorseElementsSM (implements the state machine for Morse Elements)
//The EventType field of ThisEvent will be one of: ES_Init, RisingEdge, FallingEdge, CalibrationCompleted, EOCDetected, DBButtonDown.  The parameter field of the ThisEvent will be the time that the event occurred.
//Returns ES_NO_EVENT
//Local Variables: NextState
ES_Event_t RunMorseElementsService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

//Set NextState to CurrentState
  NextState = CurrentState;
//Based on the state of the CurrentState variable choose one of the following blocks of code:

  switch (CurrentState)
  {
    case InitMorseElements:               //		CurrentState is InitMorseElements
    { if (ThisEvent.EventType == ES_INIT) //			If ThisEvent is ES_Init
      {
//			Set NextState to CalWaitForRise
        NextState = CalWaitForRise;
        puts("\n\rEntered Init State\n\r");
//			Endif
      }
    }
    break;
//		End InitMorseElements block

//		CurrentState is CalWaitForRise
    case CalWaitForRise:
    {
      if (ThisEvent.EventType == ES_RISING_EDGE)//			If ThisEvent is RisingEdge
      {
        //puts("Entered CalWaitForRise\n\r");
//				Set TimeOfLastRise to Time from event parameter
        TimeOfLastRise = ThisEvent.EventParam;
//				Set NextState to CalWaitForFall
        NextState = CalWaitForFall;
        //puts("R\n\r");
      }
//			Endif
//    If ThisEvent is CalibrationComplete
      if (ThisEvent.EventType == ES_CAL_COMPLETED)
      {
//				Set NextState to EOC_WaitRise
        //puts("Calibration Complete \r\n");
        NextState = EOC_WaitRise;
        printf("Length is %d \r\n", LengthOfDot);
      }
//			Endif
    }
    break;
//		End CalWaitForRise block

//		CurrentState is CalWaitForFall
    case CalWaitForFall:
    {
      if (ThisEvent.EventType == ES_FALLING_EDGE)//			If ThisEvent is FallingEdge
      {
        //puts("Entered CalWaitForFall\n\r");
//				Set TimeOfLastFall to Time from event parameter
        TimeOfLastFall = ThisEvent.EventParam;
//				Set NextState to CalWaitForRise
        NextState = CalWaitForRise;
//				Call TestCalibration function
        TestCalibration();
        //puts("F\n\r");
//				EndIf
      }
    }
    break;
//		End CalWaitForFall block

//		CurrentState is EOC_WaitRise
    case EOC_WaitRise:
    {
      //puts("Entered EOC_WaitRise\n\r");
//			If ThisEvent is RisingEdge
      if (ThisEvent.EventType == ES_RISING_EDGE)
      {
//				Set TimeOfLastRise to Time from event parameter
        TimeOfLastRise = ThisEvent.EventParam;
//				Set NextState to EOC_WaitFall
        NextState = EOC_WaitFall;
//				Call CharacterizeSpace function
        CharacterizeSpace();
      }
//			If ThisEvent is DBButtonDown
      if (ThisEvent.EventType == ES_DBBUTTON_DOWN)
      {
        puts("Reset\r\n");
//				Set NextState to CalWaitForRise
        NextState = CalWaitForRise;
//				Set FirstDelta to 0
        FirstDelta = 0;
      }
//			Endif
    }
    break;
//		End EOC_WaitRise block

//		CurrentState is EOC_WaitFall
    case EOC_WaitFall:
    {
      //puts("Entered EOC_WaitFall\n\r");
//			If ThisEvent is FallingEdge
      if (ThisEvent.EventType == ES_FALLING_EDGE)
      {
//				Set TimeOfLastFall to Time from event parameter
        TimeOfLastFall = ThisEvent.EventParam;
//				Set NextState to EOC_WaitRise
        NextState = EOC_WaitRise;
      }
//			If ThisEvent is DBButtonDown
      if (ThisEvent.EventType == ES_DBBUTTON_DOWN)
      {
        puts("Reset\r\n");
//				Set NextState to CalWaitForRise
        NextState = CalWaitForRise;
//				Set FirstDelta to 0
        FirstDelta = 0;
      }
//			If ThisEvent is EOCDetected
      if (ThisEvent.EventType == ES_EOC_DETECTED)
      {
//				Set NextState to DecodeWaitFall
        NextState = DecodeWaitFall;
      }
//			Endif
    }
    break;
//		End EOC_WaitFall block

//		CurrentState is DecodeWaitRise
    case DecodeWaitRise:
    {
      //puts("Entered DecodeWaitRise\n\r");
//			If ThisEvent is RisingEdge
      if (ThisEvent.EventType == ES_RISING_EDGE)
      {
//				Set TimeOfLastRise to Time from event parameter
        TimeOfLastRise = ThisEvent.EventParam;
//				Set NextState to DecodeWaitFall
        NextState = DecodeWaitFall;
//				Call CharacterizeSpace function
        CharacterizeSpace();
      }

//			If ThisEvent is DBButtonDown
      if (ThisEvent.EventType == ES_DBBUTTON_DOWN)
      {
//				Set NextState to CalWaitForRise
        NextState = CalWaitForRise;
//				Set FirstDelta to 0
        FirstDelta = 0;
      }
//			Endif
    }
    break;
//		End DecodeWaitRise block

//		CurrentState is DecodeWaitFall
    case DecodeWaitFall:
    {
      //puts("Entered DecodeWaitFall\n\r");
      //			If ThisEvent is FallingEdge
      if (ThisEvent.EventType == ES_FALLING_EDGE)
      {
//				Set TimeOfLastFall to Time from event parameter
        TimeOfLastFall = ThisEvent.EventParam;
//				Set NextState to DecodeWaitRise
        NextState = DecodeWaitRise;
        //				Call CharacterizePulse function
        CharacterizePulse();
      }
//			If ThisEvent is DBButtonDown
      if (ThisEvent.EventType == ES_DBBUTTON_DOWN)
      {
        puts("Reset\r\n");
//				Set NextState to CalWaitForRise
        NextState = CalWaitForRise;
//				Set FirstDelta to 0
        FirstDelta = 0;
      }
    }
    break;
//		End DecodeWaitFall block
  }//End Switch

//Set CurrentState to NextState
  CurrentState = NextState;
//Return ES_NO_EVENT
  return ReturnEvent;
//End of RunMorseElementsSM
}

/*****************************************************************************/
//TestCalibration
//Takes no parameters, returns nothing.
void TestCalibration(void)
{
//Local variable SecondDelta
  static uint16_t SecondDelta;
//If calibration is just starting (FirstDelta is 0)
  if (FirstDelta == 0)
  {
//		Set FirstDelta to most recent pulse width
    FirstDelta = TimeOfLastFall - TimeOfLastRise;
    //printf("FirstDelta is %d",FirstDelta);
  }
//Else
  else
  {
//		Set SecondDelta to most recent pulse width
    SecondDelta = TimeOfLastFall - TimeOfLastRise;
    //printf("SecondDelta is %d",SecondDelta);
    if ((100.0 * FirstDelta / SecondDelta) <= 33.33)
    {
//			Save FirstDelta as LengthOfDot
      LengthOfDot = FirstDelta;
//			PostEvent CalCompleted to MorseElementsSM
      //puts("\n\rPostEvent CalCompleted First to MorseElementsSM \r\n");
      ES_Event_t ThisEvent;
      ThisEvent.EventType = ES_CAL_COMPLETED;
      ThisEvent.EventParam = LengthOfDot;
      PostMorseElementsService(ThisEvent);
    }
//		ElseIf (100.0 * FirstDelta / Second Delta) greater than 300.0
    else if ((100.0 * FirstDelta / SecondDelta) >= 300.0)
    {
//			Save SecondDelta as LengthOfDot
      LengthOfDot = SecondDelta;
//			PostEvent CalCompleted to MorseElementsSM
      //puts("\n\rPostEvent CalCompleted Second to MorseElementsSM \r\n");
      ES_Event_t ThisEvent;
      ThisEvent.EventType = ES_CAL_COMPLETED;
      ThisEvent.EventParam = LengthOfDot;
      PostMorseElementsService(ThisEvent);
    }
//		Else (prepare for next pulse)
    else
    {
//			SetFirstDelta to SecondDelta
      FirstDelta = SecondDelta;
    }
//		EndIf
  }//Endelse
//EndIf
}

//Return
//End of TestCalibration
/*****************************************************************************/
//CharacterizeSpace
//Takes no parameters, returns nothing.
//Posts one of EOCDetected Event, EOWDetected Event, BadSpace Event as appropriate
//on good dot-space, does nothing
void CharacterizeSpace(void)
{
  //puts("Entered Char_Space\n\r");
//Local variable LastInterval, Event2Post
  static uint16_t LastInterval;
  //static ES_Event_t Event2Post;
//Calculate LastInterval as TimeOfLastRise - TimeOfLastFall
  LastInterval = TimeOfLastRise - TimeOfLastFall;
//If LastInterval not OK for a Dot Space
  if (LastInterval > 2 * (LengthOfDot))
  {
//	If LastInterval OK for a Character Space
    if ((LastInterval >= 3 * (LengthOfDot - 10)) && (LastInterval <= 3 * (LengthOfDot + 10)))
    {
//		PostEvent EOCDetected Event to Decode Morse Service & Morse Elements Service
      ES_Event_t ThisEvent;
      ThisEvent.EventType = ES_EOC_DETECTED;
      ThisEvent.EventParam = 1;
      //PostMorseElementsService(ThisEvent);
      ES_PostList00(ThisEvent);
      //puts("char");
    }
//	Else
    else
    {
      //		If LastInterval OK for Word Space
      if ((LastInterval >= 7 * (LengthOfDot - 10)) && (LastInterval <= 7 * (LengthOfDot + 10)))
      {
//			PostEvent EOWDetected Event to Decode Morse Service
        ES_Event_t ThisEvent;
        ThisEvent.EventType = ES_EOW_DETECTED;
        ThisEvent.EventParam = 1;
        PostMorseDecodeService(ThisEvent);
        //puts("space");
      }
      else
      {
//			PostEvent BadSpace Event to Decode Morse Service
        ES_Event_t ThisEvent;
        ThisEvent.EventType = ES_BAD_SPACE;
        ThisEvent.EventParam = 1;
        PostMorseDecodeService(ThisEvent);
      }
    }
  }
}

//Return
//End of CharacterizeSpace
/*****************************************************************************/
//CharacterizePulse
//Takes no parameters, returns nothing.
//Posts one of DotDetectedEvent, DashDetectedEvent, BadPulseEvent,
void CharacterizePulse(void)
{
  //puts("Entered Char_Pulse\n\r");
//Local variable LastPulseWidth, Event2Post
  static uint16_t LastPulseWidth;
  //static ES_Event_t Event2Post;
//Calculate LastPulseWidth as TimeOfLastFall - TimeOfLastRise
  LastPulseWidth = TimeOfLastFall - TimeOfLastRise;
//If LastPulseWidth OK for a dot
  if ((LastPulseWidth >= (LengthOfDot - 10)) && (LastPulseWidth <= (LengthOfDot + 10)))
  {
//		PostEvent DotDetected Event to Decode Morse Service
    ES_Event_t ThisEvent;
    ThisEvent.EventType = ES_DOT_DETECTED;
    //ThisEvent.EventParam = 1;
    PostMorseDecodeService(ThisEvent);
    //puts(".");
  }
//		If LastPulseWidth OK for dash
  else if ((LastPulseWidth >= 3 * (LengthOfDot - 10)) && (LastPulseWidth <= 3 * (LengthOfDot + 10)))
  {
//			PostEvent DashDetected Event to Decode Morse Service
    ES_Event_t ThisEvent;
    ThisEvent.EventType = ES_DASH_DETECTED;
    //ThisEvent.EventParam = 2;
    PostMorseDecodeService(ThisEvent);
    //puts("-");
  }
//		Else
  else
  {
//			PostEvent BadPulse Event to Decode Morse Service
    ES_Event_t ThisEvent;
    ThisEvent.EventType = ES_BAD_PULSE;
    //ThisEvent.EventParam = 0;
    PostMorseDecodeService(ThisEvent);
  }

//		EndIf
//EndIf
//Return
}

//End of CharacterizePulse
/*****************************************************************************/
