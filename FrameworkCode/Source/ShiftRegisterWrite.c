/****************************************************************************
 Module
   ShiftRegisterWrite.c

 Revision
   1.0.1

 Description
   This module acts as the low level interface to a write only shift register.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 10/11/15 19:55 jec     first pass
 10/22/18       ndc     Updated and filled formulas
****************************************************************************/
// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "BITDEFS.H"

// readability defines
#define DATA GPIO_PIN_0
#define DATA_HI BIT0HI
#define DATA_LO BIT0LO

#define SCLK GPIO_PIN_1
#define SCLK_HI BIT1HI
#define SCLK_LO BIT1LO

#define RCLK GPIO_PIN_2
#define RCLK_LO BIT2LO
#define RCLK_HI BIT2HI

#define MRSE GPIO_PIN_3
#define MRSE_LO BIT3LO
#define MRSE_HI BIT3HI

#define BTTN GPIO_PIN_4
#define BTTN_LO BIT4LO
#define BTTN_HI BIT4HI
//#define ALL_BITS (0xff << 2)

#define GET_MSB_IN_LSB(x) ((x & 0x80) >> 7)

// an image of the last 8 bits written to the shift register
static uint8_t LocalRegisterImage = 0;

// Set up Port B Ports 0-2 and initialize SR lines
void SR_Init(void)
{
  // set up port B by enabling the peripheral clock, waiting for the
  // peripheral to be ready and setting the direction
  // of PB0, PB1 & PB2 to output

  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1; //enable Port B

  while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R1) != SYSCTL_PRGPIO_R1) //wait for Port B to initialize
  {}

  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= DATA_HI;   //assign Data as I/O
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= DATA_HI;   //set Data as output
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= SCLK_HI;   //assign SCLK as I/O
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= SCLK_HI;   //set SCLK as output
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= RCLK_HI;   //assign RCLK as I/O
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= RCLK_HI;   //set RCLK as output
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= MRSE_HI;   //assign MRSE as I/O
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) &= MRSE_LO;   //set MRSE as input
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= BTTN_HI;   //assign BTTN as I/O
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) &= BTTN_LO;   //set BTTN as input

  // start with the data & sclk lines low and the RCLK line high

  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (DATA_LO & SCLK_LO);
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= RCLK_HI;
}

// Get current register value
uint8_t SR_GetCurrentRegister(void)
{
  return LocalRegisterImage;
}

// Get current register value
uint8_t SR_GetMRSE(void)
{
  uint8_t ReturnVar;
  ReturnVar = (HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) & MRSE_HI);
  return ReturnVar;
}

// Create your own function header comment
void SR_Write(uint8_t NewValue)
{
  uint8_t CountBit; //CountBit
  uint8_t BitPass;
  LocalRegisterImage = NewValue; // save a local copy

// lower the register clock
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= RCLK_LO;
  for (CountBit = 0; CountBit < 8; CountBit++)   // loop through bits in NewValue
  {
    // shift out the data while pulsing the serial clock
    // Isolate the MSB of NewValue, put it into the LSB position and output to port
    BitPass = GET_MSB_IN_LSB(NewValue);
    if (BitPass == 0)
    {
      HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT0LO;
    }
    else
    {
      HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT0HI;
    }
    // shift left once to make the next bit the MSB
    NewValue = NewValue << 1;
    HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= SCLK_HI; // raise SCLK
    HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= SCLK_LO; // lower SCLK
  }
// raise the register clock to latch the new data
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= RCLK_HI;
}
