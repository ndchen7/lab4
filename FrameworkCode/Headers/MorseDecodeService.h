/****************************************************************************
Header for MorseDecodeService
 ****************************************************************************/

#ifndef MorseDecodeService_H
#define MorseDecodeService_H
#include "ES_Events.h"

// typedefs for the states

// Public Function Prototypes
ES_Event_t RunMorseDecodeService(ES_Event_t ThisEvent);
bool InitMorseDecodeService(uint8_t Priority);
bool PostMorseDecodeService(ES_Event_t ThisEvent);

#endif /*MorseDecodeService_H */
