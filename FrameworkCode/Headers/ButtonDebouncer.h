/****************************************************************************
Header for ButtonDebouncer
 ****************************************************************************/

#ifndef ButtonDebouncer_H
#define ButtonDebouncer_H
#include "ES_Events.h"

// typedefs for the states

// State definitions for use with the query function
typedef enum { Debouncing, Ready4DB } ButtonStates_t;

// Public Function Prototypes

ES_Event_t RunButtonDebounceSM(ES_Event_t CurrentEvent);
void StartButtonDebounceSM(ES_Event_t CurrentEvent);
ButtonStates_t QueryButtonDebounceSM(void);
ES_Event_t RunButtonDebounceSM(ES_Event_t ThisEvent);
bool InitButtonDebounceSM(uint8_t Priority);
bool PostButtonDebounceSM(ES_Event_t ThisEvent);
bool Check4Button(void);

#endif /*MorseElements_H */
