//Shift Register Header File

#ifndef ShiftRegisterWrite_H
#define ShiftRegisterWrite_H

#define MRSE GPIO_PIN_3
#define MRSE_LO BIT3LO
#define MRSE_HI BIT3HI
void SR_Init(void);
uint8_t SR_GetCurrentRegister(void);
void SR_Write(uint8_t NewValue);
uint8_t SR_GetMRSE(void);
#endif
