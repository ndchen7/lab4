/****************************************************************************

  Header file for Test Harness Service0
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef MorseElementsService_H
#define MorseElementsService_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum { InitMorseElements, CalWaitForRise, CalWaitForFall,
               EOC_WaitRise, EOC_WaitFall, DecodeWaitRise, DecodeWaitFall }MorseElementsState_t;

// Public Function Prototypes

bool InitMorseElementsService(uint8_t Priority);
bool PostMorseElementsService(ES_Event_t ThisEvent);
ES_Event_t RunMorseElementsService(ES_Event_t ThisEvent);

bool CheckMorseEvents(void);

#endif /* LCDService_H */
